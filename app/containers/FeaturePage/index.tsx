/*
 * FeaturePage
 *
 * List all the features
 */
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { FormattedMessage } from 'react-intl';

import H1 from 'components/H1';
import messages from './messages';
import List from './List';
import ListItem from './ListItem';
import ListItemTitle from './ListItemTitle';

export default function FeaturePage() {
  return (
    <>
      <Helmet>
        <title>Feature Page</title>
        <meta
          name="description"
          content="Feature page of Boilerplate"
        />
      </Helmet>
    </>
  );
}
